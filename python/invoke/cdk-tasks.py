"""
Docstring
"""
import os
import requests
import json
from invoke import task

ENVIRONMENT_FILE = ".env"

ACCEPT_APP_JSON_HEADER = {"Accept": "application/json"}
CONTENT_APP_JSON_HEADER = {"Content-Type": "application/json"}

CLICKUP_API_HEADERS = {
    "Authorization": os.environ["CLICKUP_API_TOKEN"],
    **CONTENT_APP_JSON_HEADER,
}

CLICKUP_API_BASE = "https://api.clickup.com/api/v2"

INVOICE_NINJA_BASE_HEADERS = {
    **ACCEPT_APP_JSON_HEADER,
    "X-API-Token": os.environ["INVOICE_NINJA_API_TOKEN"],
    "X-Requested-With": "XMLHttpRequest",
}

INVOICE_NINJA_PUT_HEADERS = {
    **INVOICE_NINJA_BASE_HEADERS,
    **CONTENT_APP_JSON_HEADER,
}

INVOICE_NINJA_BASE_API = "https://invoicing.co/api/v1"


@task
def set_environment(cmd):
    """
    Docstring
    """
    if os.path.exists(".env"):
        cmd.run(f"source {ENVIRONMENT_FILE}")


@task(pre=[set_environment])
def deploy(cmd):
    """
    Docstring
    """
    cmd.run("cdk deploy --require-approval never")


@task(pre=[set_environment])
def destroy(cmd):
    """
    Docstring
    """
    cmd.run("cdk destroy --force")


@task(pre=[set_environment, destroy, deploy])
def replace(cmd):
    """
    Docstring
    """
    return cmd.run("")


@task(pre=[set_environment])
def bootstrap(cmd):
    """
    Docstring
    """
    current_sts_account_result = cmd.run(
        "aws sts get-caller-identity | jq -r '.Account'", hide=True
    )
    account_string = current_sts_account_result.tail(stream="stdout", count=1).strip()
    region = os.getenv("AWS_DEFAULT_REGION")
    cmd.run(f"cdk bootstrap aws://{account_string}/{region}")


@task(pre=[set_environment])
def black(cmd):
    """
    Docstring
    """
    cmd.run("black .")


@task(pre=[set_environment])
def lint(cmd):
    """
    Docstring
    """
    cmd.run("pylint *.py ./cdk/invoice_ninja_clickup_integration/ --fail-under 9")


@task(pre=[set_environment])
def test(cmd):
    """
    Docstring
    """
    cmd.run("PYTHONPATH=. pytest")


@task(pre=[set_environment, black, lint, test])
def pre_commit(cmd):
    """
    Docstring
    """
    pass


@task
def get_clickup_lists_info_from_clients_folder(cmd):
    """
    Docstring
    """
    teams_response = requests.get(
        url=f"{CLICKUP_API_BASE}/team",
        headers=CLICKUP_API_HEADERS,
    )
    # print(teams_response.text) teams['teams'][0][id] = 14150842

    team_id = json.loads(teams_response.text)["teams"][0]["id"]

    spaces_response = requests.get(
        url=f"{CLICKUP_API_BASE}/team/{team_id}/space?archived=false",
        headers=CLICKUP_API_HEADERS,
    )

    for space in json.loads(spaces_response.text)["spaces"]:
        if space["name"] == "Clients":
            space_id = space["id"]

    folders_response = requests.get(
        url=f"{CLICKUP_API_BASE}/space/{space_id}/folder?archived=false",
        headers=CLICKUP_API_HEADERS,
    )

    clickup_client_lists = []
    for folder in json.loads(folders_response.text)["folders"]:
        lists_response = requests.get(
            url=f"{CLICKUP_API_BASE}/folder/{folder['id']}/list?archived=false",
            headers=CLICKUP_API_HEADERS,
        )
        lists_obj = json.loads(lists_response.text)["lists"]
        assert len(lists_obj) == 1
        clickup_client_lists.append((lists_obj[0]["name"], lists_obj[0]["id"]))
    print(clickup_client_lists)


@task
def get_invoice_ninja_client_id(cmd):
    """
    Docstring
    """
    clients_data = json.loads(
        requests.get(
            url=f"{INVOICE_NINJA_BASE_API}/clients",
            headers=INVOICE_NINJA_BASE_HEADERS,
        ).text
    )["data"]

    clients = []
    for client in clients_data:
        clients.append((client["name"], client["id"]))
    print(clients)


@task
def update_aws_param_for_clickup_lists_and_invoice_ninja_clients(cmd):
    """
    Docstring
    """
    # TODO - implement, this uses the other get commands and posts to aws ssm
    pass
